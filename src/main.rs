use std::{env, io::{self, prelude::*}};

fn main() -> Result<(), Box<std::error::Error>> {
    let stdout = io::stdout();
    let mut stdout = stdout.lock();
    let stdin = io::stdin();
    let mut stdin = stdin.lock();

    let float = match env::args().skip(1).next() {
        Some(float) => float,
        None => {
            write!(stdout, "Input: ")?;
            stdout.flush()?;

            let mut line = String::new();
            stdin.read_line(&mut line)?;
            line
        }
    };
    let float: f64 = float.trim().parse()?;

    write!(stdout, "Binary: ")?;

    {
        if float < 0.0 {
            write!(stdout, "-")?;
        }
        let mut float = float.abs();

        let mut cursor = 2f64.powi(std::f64::MAX_EXP - 1);
        while cursor > 0.0 {
            let divided = float / cursor;
            if divided.floor() > 0.0 {
                stdout.write_all(&[b'0' + (divided % 2.0) as u8])?;
            }
            cursor = (cursor / 2.0).floor();
        }
        if float % 1.0 > 0.0 {
            write!(stdout, ".")?;
            loop {
                float *= 2.0;
                stdout.write_all(&[b'0' + (float % 2.0) as u8])?;
                if float % 1.0 == 0.0 {
                    break;
                }
            }
        }
    }
    write!(stdout, "\n\n")?;

    let bits = float.to_bits();

    let sign = bits >> 63;
    let exp  = (bits >> 52 & 0x7FF) as i16;
    let mantissa = bits & 0xFFFFFFFFFFFFF;

    let sign_human = 1 - sign as i8*2;
    let exp_human = if exp == 0 { 0 } else { exp - 0x3FF };
    let mantissa_human = 1.0 + mantissa as f64 / 2f64.powi(52);

    writeln!(stdout, "Bits:  {:01b} {:011b} {:052b}", sign, exp, mantissa)?;
    writeln!(stdout, "      {:2} {:11} {:52}", sign_human, exp_human, mantissa_human)?;
    writeln!(stdout, "       ^ ^^^^^^^^^^^ ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")?;
    writeln!(stdout, "       |     exp                           mantissa")?;
    writeln!(stdout, "       - sign")?;

    writeln!(stdout, "Equation: {} * 2^{} * {}", sign_human, exp_human, mantissa_human)?;

    Ok(())
}
