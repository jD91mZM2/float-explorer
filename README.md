# float-explorer

A program I created in order to explore how floats work. You enter a float and
it outputs the memory representation for that float, and an equation for
calculating the original value.

![Screenshot](https://i.imgur.com/ywpveWj.png)
